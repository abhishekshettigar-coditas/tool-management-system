export const toolsContent = () => {
    return (`
    <h1 class='content-header'>TOOLS</h1>
    <button type='button' class='action-button create-button' id='tools-create-button'>Create <i class="fa-solid fa-plus"></i></button>

                <table class="table" id='tools-table'>

                    <thead>

                        <tr>
                            <th>Tool ID</th>
                            <th>Tool Name</th>
                            <th>Tool Size</th>
                            <th>Tool Price</th>
                            <th>Update / Delete</th>
                        </tr>

                    </thead>


                </table>
    `)
}



