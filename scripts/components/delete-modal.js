export const deleteModal = () => {
    return (`
    
        <section id="modal-container">

            <div class="modal">

            <h1 class='modal-header'>Delete</h1>

            <h2>Are you sure, you want to delete?</h2>

            <button type="button" class="yes-button">Yes</button>
            <button type="button" class="no-button">No</button>

        </form>
            </div>
        </section>

    `)
}