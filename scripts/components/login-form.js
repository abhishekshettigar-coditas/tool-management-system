export const loginForm = () => {
    return (`

        <section class="login-section" id='login-section'>

            <img src="./img/login image.png" alt="login-image" class="login-image">
            <h3 class="login-header">WELCOME</h3>

            <form class="login-form">
                <label class="field-labels"><i class="fa-solid fa-user"></i>&nbsp;&nbsp;Username</label>
                <input type="text" id="username" class="login-fields" required>
                <label class="field-labels"><i class="fa-solid fa-lock"></i>&nbsp;&nbsp;Password</label>
                <input type="text" id="password" class="login-fields" required>
                <button type="submit" id="login-button">Login</button>
            </form>

        </section>
        
    `)
}