export const createToolModal = () => {
    return (`
    
        <section id="modal-container">

            <div class="modal">

            <h1 class='modal-header'>Create Tool</h1>

            <form action="https://e18b-103-169-241-128.in.ngrok.io/admin/createTool" method='POST' class="form">

            <label for="">Enter the Tool Name</label>
            <input type="text" id='tool-name' required>

            <label for="">Enter the Tool Size</label>
            <input type="number" id='tool-size' required>

            <label for="">Enter the Tool Price</label>
            <input type="number" id='tool-price' required>

            <button type="button" class="form-button" id='create-tool-submit-button'>Submit</button>

        </form>
            </div>
        </section>

    `)
}