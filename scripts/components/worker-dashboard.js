export const workerDashboard = () => {
    return (`
    <section class="dashboard">

        <div class="sidebar">
            <h3 class="sidebar-header">Options</h3>
            <button type="button" class="selections" id='tools-button'>Create Order</button>
            <button type="button" class="selections" id='workers-button'>Veiw Orders</button>
            <button type="button" class="selections" id='orders-button'>Update Order Status</button>
        </div>

        <div class="dashboard-content" id="dashboard-content"></div>

    </section>
    `)
}