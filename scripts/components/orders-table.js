export const ordersContent = () => {
    return (`
    <h1 class='content-header'>ORDERS</h1>

                <table class="table" id='orders-table'>

                    <thead>

                        <tr>
                            <th>Order ID</th>
                            <th>Worker Name</th>
                            <th>Customer Name</th>
                            <th>Ordered Tool</th>
                            <th>Tool size</th>
                            <th>Total Price</th>
                            <th>Order Status</th>

                        </tr>

                    </thead>


                </table>
    `)
}