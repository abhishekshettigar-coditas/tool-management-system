export const workersContent = () => {
    return (`
    <h1 class='content-header'>WORKERS</h1>
    <button type='button' class='action-button create-button' id='workers-create-button'>Create <i class="fa-solid fa-plus"></i></button>

                <table class="table" id='workers-table'>

                    <thead>

                        <tr>
                        <th>Worker ID</th>
                        <th>Worker Name</th>
                        <th>Worker Username</th>
                        <th>Worker Salary</th>
                        <th>Update/Delete</th>
                        </tr>

                    </thead>


                </table>
`)
}