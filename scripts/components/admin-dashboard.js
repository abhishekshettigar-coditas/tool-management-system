export const adminDashboard = () => {
    return (`
    <section class="dashboard">

        <div class="sidebar">
            <h3 class="sidebar-header">Options</h3>
            <button type="button" class="selections" id='tools-button'>Tools</button>
            <button type="button" class="selections" id='workers-button'>Workers</button>
            <button type="button" class="selections" id='orders-button'>Orders</button>
        </div>

        <div class="dashboard-content" id="dashboard-content"></div>

    </section>
    `)
}