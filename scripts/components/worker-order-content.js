export const workerOrderContent = () => {
    return (`
    <h1 class='content-header'>ORDERS</h1>

                <table class="table" id='worker-order-table'>

                    <thead>

                        <tr>
                        <th>Order ID</th>
                        <th>Customer Name</th>
                        <th>Ordered Tool</th>
                        <th>Order Size</th>
                        <th>Order Price</th>
                        <th>Order Status</th>
                        </tr>

                    </thead>


                </table>
`)
}