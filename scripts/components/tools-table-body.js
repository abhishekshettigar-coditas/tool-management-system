export const toolsTableBody = (tool) => {
    return (`

                        <tr>
                            <td>${tool.toolId} </td>
                            <td>${tool.toolName} </td>
                            <td>${tool.toolSize} </td>
                            <td>${tool.toolPrice} </td>
                            <td><button type='button' class='action-button update-tools' id='${tool.toolId}'><i class="fa-solid fa-pen-to-square"></i></button>/<button type='button' class='action-button delete-tools' id='${tool.toolId}'><i class="fa-solid fa-trash"></i></button></td>
                        </tr>

    `)
}