export const workersTableBody = (worker) => {
    return (`

                        <tr>
                            <td>${worker.workerId} </td>
                            <td>${worker.workerName} </td>
                            <td>${worker.workerUsername} </td>
                            <td>${worker.workerSalary} </td>
                            <td><button type='button' class='action-button update-worker' id=${worker.workerId}><i class="fa-solid fa-pen-to-square"></i></button>/<button type='button' class='action-button delete-worker' id=${worker.workerId}><i class="fa-solid fa-trash"></i></button></td>
                        </tr>

                
    `)
}