export const createWorkerModal = () => {
    return (`
    
        <section id="modal-container">

            <div class="modal">

            <h1 class='modal-header'>Create Worker</h1>

            <form action="https://e18b-103-169-241-128.in.ngrok.io/admin/createWorker" method='POST' class="form">

            <label for="worker-name">Enter the Worker Name</label>
            <input type="text" id='worker-name' required>

            <label for="worker-username">Enter the Worker Username</label>
            <input type="text" id='worker-username' required>

            <label for="worker-password">Enter the Worker Password</label>
            <input type="password" id='worker-password' required>
            
            <label for="worker-salary">Enter the Worker Salary</label>
            <input type="number" id='worker-salary' required>

            <button type="button" class="form-button" id='create-worker-submit-button'>Submit</button>

        </form>
            </div>
        </section>

    `)
}