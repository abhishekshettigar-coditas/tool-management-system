import { $ } from './dom.js';

const mainContent = $('.main-content');
const htmlBody = $('body');
const modalContent = $('.modal-content');


export default {
    mainContent,
    htmlBody,
    modalContent
}