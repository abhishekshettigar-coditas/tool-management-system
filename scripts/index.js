import references from "./reference.js"
import { loginForm } from "./components/login-form.js";
import { $ } from "./dom.js";
import http from "./http.js";
import { adminDashboard } from "./components/admin-dashboard.js";
import { toolsContent } from "./components/tools-table.js";
import { workersContent } from "./components/worker-table.js";
import { toolsTableBody } from "./components/tools-table-body.js";
import { workersTableBody } from "./components/workers-table-body.js";
import { ordersContent } from "./components/orders-table.js";
import { ordersTableBody } from "./components/orders-table-body.js";
import { createToolModal } from "./components/create-tool-modal.js";
import { deleteModal } from "./components/delete-modal.js";
import { createWorkerModal } from "./components/create-worker-modal.js";
import { workerDashboard } from "./components/worker-dashboard.js";
import { workerOrderContent } from "./components/worker-order-content.js";

const { mainContent, modalContent } = references;

const displayForm = () => {
    mainContent[0].innerHTML = loginForm();
}
displayForm();

const loginButton = $('#login-button');


const login = async (event) => {
    event.preventDefault();
    const userName = $('#username');
    const password = $('#password')
    const bodyData = {
        "username": `${userName.value}`,
        "password": password.value
    }
    try {

        const response = await fetch('https://e18b-103-169-241-128.in.ngrok.io/authenticate', {
            method: 'POST',
            headers: {
                "ngrok-skip-browser-warning": "1234",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyData)
        })
        const responseToken = await response.json();
        const token = responseToken.jwtToken;
        const role = responseToken.userRole;
        const userId = responseToken.userId;
        window.localStorage.setItem('token', token);

        if (role === 'ADMIN') {
            return displayAdminDashBoard(event);
        }

        return displayWorkersDashboard(event, userId);
        // return sendToken(token);
    } catch (error) {
        throw error;
    }

}

// ADMIN DASHBOARD

const displayAdminDashBoard = (event) => {
    // const loginSection = $('#login-section');
    mainContent[0].innerHTML = adminDashboard();
    const toolsButton = $('#tools-button');
    const workersButton = $('#workers-button');
    const ordersButton = $('#orders-button');
    const adminContent = $('#dashboard-content');
    toolsButton.addEventListener('click', () => displayTools(adminContent));
    workersButton.addEventListener('click', () => displayWorkers(adminContent));
    ordersButton.addEventListener('click', () => displayOrders(adminContent));
}

loginButton.addEventListener('click', (event) => login(event))


// ADMIN TOOLS
// DISPLAY TOOLS
const displayTools = (adminContent) => {
    adminContent.innerHTML = '';
    adminContent.innerHTML = toolsContent();

    const toolsTable = $('#tools-table');
    const toolsCreateButton = $('#tools-create-button');

    const displayToolsTableRow = async () => {
        try {
            const responseTools = await http.get('admin/getTools');

            for (let tools of responseTools) {
                const toolsTableRow = document.createElement('tbody');
                toolsTableRow.innerHTML += toolsTableBody(tools);
                toolsTable.appendChild(toolsTableRow);
            }

        } catch (error) {
            throw error;
        }
    }
    displayToolsTableRow();
    toolsCreateButton.addEventListener('click', () => createToolPopup(adminContent))
    toolsTable.addEventListener('click', (event) => toolsButtons(event, adminContent));
}


// CREATE TOOL

const createToolPopup = (adminContent) => {

    modalContent[0].innerHTML = createToolModal();
    const createToolSubmitButton = $('#create-tool-submit-button');
    createToolSubmitButton.addEventListener('click', () => submitTools())

    const submitTools = () => {

        const toolName = $('#tool-name').value;
        const toolsize = $('#tool-size').value;
        const toolPrice = $('#tool-price').value;

        const bodyData = {
            "toolName": `${toolName}`,
            "toolSize": Number.parseInt(`${toolsize}`),
            "toolPrice": Number.parseInt(`${toolPrice}`)
        }

        const toolsData = http.post('admin/createTool', bodyData);
        modalContent[0].innerHTML = '';
        displayTools(adminContent);
    }
}


// DELETE TOOL

const DeleteToolPopup = (selectedId, selectedClass, adminContent) => {
    modalContent[0].innerHTML = deleteModal();
    const yesButton = $('.yes-button');
    const noButton = $('.no-button');
    yesButton[0].addEventListener('click', () => deleteTools())
    noButton[0].addEventListener('click', () => modalContent[0].innerHTML = '')

    const deleteTools = () => {
        const deleteTool = http.delete(`admin/deleteTool/${selectedId}`);
        modalContent[0].innerHTML = '';
        displayTools(adminContent);
    }

}


// UPDATE TOOL

const updateToolPopup = async (selectedId, selectedClass, adminContent) => {
    modalContent[0].innerHTML = createToolModal();
    const getToolDetails = await http.get('admin/getTools');

    const addToolsPopupDetails = (tools) => {
        $('#tool-name').value = tools.toolName;
        $('#tool-size').value = tools.toolSize;
        $('#tool-price').value = tools.toolPrice;

        const createToolSubmitButton = $('#create-tool-submit-button');
        createToolSubmitButton.addEventListener('click', () => updateTools())
    }

    const updateTools = async () => {

        const toolName = $('#tool-name').value;
        const toolsize = $('#tool-size').value;
        const toolPrice = $('#tool-price').value;

        const bodyData = {

            'toolId': selectedId,
            "toolName": `${toolName}`,
            "toolSize": Number.parseInt(`${toolsize}`),
            "toolPrice": Number.parseInt(`${toolPrice}`)
        }

        const putTools = await http.put('admin/updateTool', bodyData);
        modalContent[0].innerHTML = '';
    }

    for (let tools of getToolDetails) {
        if (tools.toolId === selectedId) {
            addToolsPopupDetails(tools);
        }
    }

}


// TOOLS CRUD BUTTONS
const toolsButtons = (event, adminContent) => {
    const selectedId = Number.parseInt(event.target.id);
    const selectedClass = event.target.className;
    console.log(selectedClass, selectedId)
    if (selectedClass === 'action-button delete-tools') {
        DeleteToolPopup(selectedId, selectedClass, adminContent);
    }
    if (selectedClass === 'action-button update-tools') {
        updateToolPopup(selectedId, selectedClass, adminContent);
    }
}


// ADMIN WORKERS
// DISPLAY WORKER

const displayWorkers = (adminContent) => {
    adminContent.innerHTML = '';
    adminContent.innerHTML = workersContent();

    const workersTable = $('#workers-table');
    const workersCreateButton = $('#workers-create-button');

    const displayWorkersTableRow = async () => {
        try {
            const responseWorkers = await http.get('admin/getWorkers');

            for (let workers of responseWorkers) {
                const workersTableRow = document.createElement('tbody');
                workersTableRow.innerHTML += workersTableBody(workers);
                workersTable.appendChild(workersTableRow);
            }
        } catch (error) {
            throw error;
        }
    }
    displayWorkersTableRow();
    workersCreateButton.addEventListener('click', () => createWorkerPopup(adminContent))
    workersTable.addEventListener('click', (event) => workersButtons(event, adminContent));

}


// CREATE WORKER
const createWorkerPopup = (adminContent) => {

    modalContent[0].innerHTML = createWorkerModal();
    const createWorkerSubmitButton = $('#create-worker-submit-button');
    createWorkerSubmitButton.addEventListener('click', () => submitWorker())

    const submitWorker = () => {

        const workerName = $('#worker-name').value;
        const workerUsername = $('#worker-username').value;
        const workerPassword = $('#worker-password').value;
        const workerSalary = $('#worker-salary').value;

        const bodyData = {
            "workerName": (`${workerName}`),
            "workerUsername": `${workerUsername}`,
            "workerPassword": Number.parseInt(`${workerPassword}`),
            "workerSalary": Number.parseInt(`${workerSalary}`)
        }

        const workersData = http.post('admin/createWorker', bodyData);
        modalContent[0].innerHTML = '';
        return displayWorkers(adminContent);
    }
}


// DELETE WORKER

const deleteWorkerPopup = (selectedId, selectedClass, adminContent) => {
    modalContent[0].innerHTML = deleteModal();
    const yesButton = $('.yes-button');
    const noButton = $('.no-button');
    yesButton[0].addEventListener('click', () => deleteWorkers())

    const deleteWorkers = () => {
        const deleteWorker = http.delete(`admin/deleteWorker/${selectedId}`);
        displayWorkers(adminContent);
        modalContent[0].innerHTML = '';

    }

    noButton[0].addEventListener('click', () => modalContent[0].innerHTML = '')
}

// UPDATE WORKER

const updateWorkerPopup = async (selectedId, selectedClass, adminContent) => {
    modalContent[0].innerHTML = createWorkerModal();
    const getWorkerDetails = await http.get('admin/getWorkers');

    const addWorkersPopupDetails = (workers) => {
        $('#worker-name').value = workers.workerName;
        $('#worker-username').value = workers.workerUsername;
        $('#worker-password').value = workers.workerPassword;
        $('#worker-salary').value = workers.workerSalary;

        const createWorkerSubmitButton = $('#create-worker-submit-button');
        createWorkerSubmitButton.addEventListener('click', () => updateWorkers())
    }

    const updateWorkers = async (selectedId) => {
        console.log(selectedId);

        const workerName = $('#worker-name').value;
        const workerUsername = $('#worker-username').value;
        const workerPassword = $('#worker-password').value;
        const workerSalary = $('#worker-salary').value;

        const bodyData = {

            "workerId": selectedId,
            "workerName": (`${workerName}`),
            "workerUsername": `${workerUsername}`,
            "workerPassword": Number.parseInt(`${workerPassword}`),
            "workerSalary": Number.parseInt(`${workerSalary}`),
            "worker_orders": []
        }

        modalContent[0].innerHTML = '';
        const workersData = http.put('admin/updateWorker', bodyData);
        displayWorkers(adminContent);
    }

    for (let workers of getWorkerDetails) {
        if (workers.workerId === selectedId) {
            return addWorkersPopupDetails(workers);
        }
    }

}


// WORKERS CRUD BUTTONS

const workersButtons = (event, adminContent) => {
    const selectedId = Number.parseInt(event.target.id);
    const selectedClass = event.target.className;
    console.log(selectedId, selectedClass);
    if (selectedClass === 'action-button delete-worker') {
        deleteWorkerPopup(selectedId, selectedClass, adminContent);
    }
    if (selectedClass === 'action-button update-worker') {
        updateWorkerPopup(selectedId, selectedClass, adminContent);
    }
}


// ORDERS

const displayOrders = (adminContent) => {
    adminContent.innerHTML = '';
    adminContent.innerHTML = ordersContent();

    const ordersTable = $('#workers-table');

    const displayOrdersTableRow = async () => {
        try {
            const responseOrders = await http.get('admin/getOrders');
            console.log(responseOrders);

            for (let orders of responseOrders) {
                const ordersTableRow = document.createElement('tbody');
                ordersTableRow.innerHTML += ordersTableBody(orders);
                ordersTable.appendChild(ordersTableRow);
            }
        } catch (error) {
            throw error;
        }
    }
    displayOrdersTableRow();
}


// WORKERS DASHBOARD

const displayWorkersDashboard = (event, userId) => {

    mainContent[0].innerHTML = workerDashboard();

    const createOrderButton = $('#tools-button');
    const viewOrdersButton = $('#workers-button');
    const updateOrderStatusButton = $('#orders-button');
    const workerContent = $('#dashboard-content');

    createOrderButton.addEventListener('click', () => createOrder(workerContent));
    viewOrdersButton.addEventListener('click', () => displayWorkerOrders(workerContent, userId));
    updateOrderStatusButton.addEventListener('click', () => updateStatus(workerContent));

}

const displayWorkerOrders = async (workerContent, userId) => {
    workerContent.innerHTML = '';
    workerContent.innerHTML = workerOrderContent();

    try {
        const responseWorkerOrders = await http.get(`worker/getWorkerOrders/${userId}`);
        console.log(responseWorkerOrders);

        // for (let orders of responseWorkerOrders) {
        //     const workerOrderTableRow = document.createElement('tbody');
        //     workerOrderTableRow.innerHTML += workersTableBody(workers);
        //     workersTable.appendChild(workersTableRow);
        // }
    } catch (error) {
        throw error;
    }
}